const menuLevelTwo = document.querySelector('.left-bar__games__nav-level-two');
const arrNavItemContent = document.querySelectorAll('.nav-item__content');

arrNavItemContent.forEach(item => {
    item.addEventListener('click', (event) => {
        // анимируем стрелки у пунктов меню
        let arrow = event.currentTarget.querySelector('svg');
       
        if (arrow.classList.contains('active-arrow')) {
            arrow.style.transform = 'rotate(180deg)';
        } else {
            arrow.style.transform = 'rotate(0deg)';
        }
        arrow.classList.toggle('active-arrow');

        let itemNav = event.currentTarget;
        if (itemNav.classList.contains('for-nav-level-two')) {
            
            if (menuLevelTwo.classList.contains('active-nav')) {
                menuLevelTwo.style.display = 'none';
            } else {
                menuLevelTwo.style.display = 'block';
            }
            menuLevelTwo.classList.toggle('active-nav');
        }
    }, false);
});

